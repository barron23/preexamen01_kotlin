package com.example.preexamen_kotlin

class ReciboNomina    // MÉTODOS
    (// Set & Get
    var numRecibo: Int,
    var nombre: String,
    var horasTrabNormal: Int,
    var horasTrabExtras: Int,
    var puesto: Int,
    var impuestoPorc: Int
) {

    // Calcular subtotal
    fun calcularSubtotal(puesto: Int, horasTrabNormal: Float, horasTrabExtras: Float): Float {
        var pagoBase = 200f
        var total = 0f
        // Pago segun el puesto
        if (puesto == 1) {
            pagoBase = (pagoBase + pagoBase * 0.20).toFloat()
        } else if (puesto == 2) {
            pagoBase = (pagoBase + pagoBase * 0.50).toFloat()
        } else if (puesto == 3) {
            pagoBase = (pagoBase + pagoBase * 1.00).toFloat()
        } else {
            println("No se puede calcular")
        }

        // Horas extras - pago doble
        total = pagoBase * horasTrabNormal + horasTrabExtras * pagoBase * 2
        return total
    }

    // impuesto
    fun calcularImpuesto(
        puesto: Int,
        horasTrabNormal: Float,
        horasTrabExtras: Float
    ): Float {
        return (calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras) * 0.16).toFloat()
    }

    // total
    fun calcularTotal(subtotal: Float, impuesto: Float): Float {
        return subtotal - impuesto
    }
}