package com.example.preexamen_kotlin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class ReciboNominaActivity : AppCompatActivity() {
    // Declarar variables
    private var txtNumeroRecibo: EditText? = null
    private var txtNombre: EditText? = null
    private var txtHorasTrabajadasNormales: EditText? = null
    private var txtHorasTrabajadasExtra: EditText? = null
    private var rbAuxiliar: RadioButton? = null
    private var rbAlbañil: RadioButton? = null
    private var rbIngObra: RadioButton? = null
    private var lblUsuario: TextView? = null
    private var txtSubtotal: EditText? = null
    private var txtImpuesto: EditText? = null
    private var txtTotalPagar: EditText? = null
    private var btnCalcular: Button? = null
    private var btnLimpiar: Button? = null
    private var btnRegresar: Button? = null
    private var recibo: ReciboNomina? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo_nomina)
        iniciarComponentes()

        // Obtener los datos del Main Activity
        val datos = intent.extras
        val nombre = datos!!.getString("nombre")
        lblUsuario!!.text = nombre
        btnCalcular!!.setOnClickListener { calcular() }
        btnLimpiar!!.setOnClickListener { limpiar() }
        btnRegresar!!.setOnClickListener { regresar() }
    }

    private fun iniciarComponentes() {
        txtNumeroRecibo = findViewById<View>(R.id.txtNumeroRecibo) as EditText
        txtNombre = findViewById<View>(R.id.txtNombre) as EditText
        txtHorasTrabajadasNormales = findViewById<View>(R.id.txtHorasTrabajadas) as EditText
        txtHorasTrabajadasExtra = findViewById<View>(R.id.txtHorasTrabajadasExtra) as EditText
        txtSubtotal = findViewById<View>(R.id.txtSubtotal) as EditText
        txtImpuesto = findViewById<View>(R.id.txtImpuesto) as EditText
        txtTotalPagar = findViewById<View>(R.id.txtTotalPagar) as EditText
        lblUsuario = findViewById<View>(R.id.lblUsuario) as TextView
        rbAuxiliar = findViewById<View>(R.id.radAuxiliar) as RadioButton
        rbAlbañil = findViewById<View>(R.id.radAlbañil) as RadioButton
        rbIngObra = findViewById<View>(R.id.radIngObra) as RadioButton
        btnCalcular = findViewById<View>(R.id.btnCalcular) as Button
        btnLimpiar = findViewById<View>(R.id.btnLimpiar) as Button
        btnRegresar = findViewById<View>(R.id.btnRegresar) as Button
        recibo = ReciboNomina(0, "", 0, 0, 0, 0)
        bloqueartextos()
    }

    private fun calcular() {
        //validaciones
        var puesto = 1
        var horasNormales = 0f
        var horasExtras = 0f
        var subtotal = 0f
        var impuesto = 0f
        var total = 0f
        if (rbAuxiliar!!.isChecked) {
            puesto = 1
        } else if (rbAlbañil!!.isChecked) {
            puesto = 2
        } else if (rbIngObra!!.isChecked) {
            puesto = 3
        }
        if (txtNumeroRecibo!!.text.toString().isEmpty()) {
            Toast.makeText(this, "Por favor, Ingrese su Número de Recibo", Toast.LENGTH_SHORT)
                .show()
        } else if (txtNombre!!.text.toString().isEmpty()) {
            Toast.makeText(this, "Por favor, Ingrese su Nombre", Toast.LENGTH_SHORT).show()
        } else if (txtHorasTrabajadasNormales!!.text.toString()
                .isEmpty() || txtHorasTrabajadasExtra!!.text.toString().isEmpty()
        ) {
            if (txtHorasTrabajadasNormales!!.text.toString().isEmpty()) {
                Toast.makeText(this, "Ingrese la Cantidad de horas Trabajadas", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(this, "Ingrese la Cantidad de Horas Extras", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        //Correcciones de validaciones
        if (!txtHorasTrabajadasNormales!!.text.toString().isEmpty()) {
            horasNormales = txtHorasTrabajadasNormales!!.text.toString().toFloat()
        }
        if (!txtHorasTrabajadasExtra!!.text.toString().isEmpty()) {
            horasExtras = txtHorasTrabajadasExtra!!.text.toString().toFloat()
        }
        if (!txtHorasTrabajadasNormales!!.text.toString().isEmpty() && !txtHorasTrabajadasExtra!!.text.toString().isEmpty() && puesto != 0
        ) {
            subtotal = recibo!!.calcularSubtotal(puesto, horasNormales, horasExtras)
            impuesto = recibo!!.calcularImpuesto(puesto, horasNormales, horasExtras)
            total = recibo!!.calcularTotal(subtotal, impuesto)
            txtSubtotal!!.setText("" + subtotal)
            txtImpuesto!!.setText("" + impuesto)
            txtTotalPagar!!.setText("" + total)
            println("subtotal = $subtotal")
            println("impuesto = $impuesto")
            println("total = $total")
        }
    }

    private fun limpiar() {
        txtNumeroRecibo!!.setText("")
        txtNombre!!.setText("")
        txtHorasTrabajadasNormales!!.setText("")
        txtHorasTrabajadasExtra!!.setText("")
        txtSubtotal!!.setText("")
        txtImpuesto!!.setText("")
        txtTotalPagar!!.setText("")
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Recibo de Nomina")
        confirmar.setMessage("Regresar al MainActivity")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialog, which -> finish() }
        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialog, which ->
            // No hace nada
        }
        confirmar.show()
    }

    //Bloquear edittexts de resultados
    private fun bloqueartextos() {
        txtSubtotal!!.isFocusable = false
        txtImpuesto!!.isFocusable = false
        txtTotalPagar!!.isFocusable = false
    }

}