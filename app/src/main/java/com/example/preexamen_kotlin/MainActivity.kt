package com.example.preexamen_kotlin

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private var lblNombreTrabajador: TextView? = null
    private var txtNombreTrabajador: EditText? = null
    private var btnEntrar: Button? = null
    private var btnSalir: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnEntrar!!.setOnClickListener { entrar() }
        btnSalir!!.setOnClickListener { salir() }
    }

    private fun iniciarComponentes() {
        lblNombreTrabajador = findViewById<View>(R.id.lblNombreTrabajador) as TextView
        txtNombreTrabajador = findViewById<View>(R.id.txtNombreTrabajador) as EditText
        btnEntrar = findViewById<View>(R.id.btnEntrar) as Button
        btnSalir = findViewById<View>(R.id.btnSalir) as Button
    }

    private fun entrar() {
        val strNombre: String

        //Relacionar lo que introduce el usuario desde login con los valores del archivo values > string.xml
        strNombre = applicationContext.resources.getString(R.string.nombre)

        //VALIDAR LOGIN
        if (strNombre == txtNombreTrabajador!!.text.toString().trim { it <= ' ' }) {

            //Hacer el paquete para enviar la informacion
            val bundle = Bundle()
            bundle.putString("nombre", txtNombreTrabajador!!.text.toString())

            //Hacer intent para llamar otra actividad
            //                                       origen              destino         de la peticion
            val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
            //enviando el paquete
            intent.putExtras(bundle)

            //Iniciar la actividad - esperando o no respuesta
            startActivity(intent)
        } else {
            Toast.makeText(
                this.applicationContext,
                "El usuario o contraseña no son válido",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun salir() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("PREEXAMEN")
        confirmar.setMessage("¿Desea cerrar la app?")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialog: DialogInterface?, which: Int -> finish() }
        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialog: DialogInterface?, which: Int -> }
        confirmar.show()
    }
}